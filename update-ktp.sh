#!/bin/bash

if [ `pwd` != $HOME/ktp ]
then
    echo "This must be run from your ~/ktp directory"
    exit
fi

./kdesrc-build/kdesrc-build telepathyqt kpeople ktp
