#!/bin/bash

echo "This script will set up a KTp development environment using kdesrc-build"
echo "All source files will be kept in ~/ktp/src built in ~/ktp/build and installed to ~/ktp/install"
echo "~/ktp/install will be added to the relevant search paths"

read -p "Press [Enter] key to continue..."

mkdir -p ~/ktp/
cp ./kdesrc-buildrc ~/ktp/.


# If anyone comes up with a better way to do this without sudo _that works_
# Please let me know

# Most other things do not.
echo "As we use DBus activation we need to include the new environment on login"
echo "To do this we copy the environment setup script to /etc/profile.d"
echo "This requires root rights"
echo
echo "sudo cp ktp-env.sh /etc/profile.d/ktp-env.sh"
sudo cp ktp-env.sh /etc/profile.d/ktp-env.sh
sudo -k #Revoke sudo rights

cp update-ktp.sh ~/ktp/update-ktp
chmod u+x ~/ktp/update-ktp

cd ~/ktp/
echo "Downloading kdesrc-build"
git clone git://anongit.kde.org/kdesrc-build

./update-ktp

echo "Setup complete."
echo "Please log out and log in again to continue."

echo "To update or fix failures run ./update-ktp from inside your ~/ktp directory"
