export KDEDIRS=$HOME/ktp/install
export LD_LIBRARY_PATH=$KDEDIRS/lib:$KDEDIRS/lib64:$QTDIR/lib:$LD_LIBRARY_PATH
export XDG_DATA_DIRS=$KDEDIRS/share:$XDG_DATA_DIRS:/usr/share
export XDG_CONFIG_DIRS=$KDEDIRS/etc/xdg:$XDG_CONFIG_DIRS:/etc/xdg
export PATH=$KDEDIRS/bin:$QTDIR/bin:$PATH
export QT_PLUGIN_PATH=$KDEDIRS/lib/plugins:$KDEDIRS/lib64/plugins:$KDEDIRS/lib/x86_64-linux-gnu/plugins:$KDEDIRS/lib64/x86_64-linux-gnu/plugins:$QT_PLUGIN_PATH
export QML2_IMPORT_PATH=$KDEDIRS/lib/qml:$KDEDIRS/lib64/qml:$KDEDIRS/lib/x86_64-linux-gnu/qml:$KDEDIRS/lib64/x86_64-linux-gnu/plugins:$QTDIR/qml:$QML2_IMPORT_PATH
